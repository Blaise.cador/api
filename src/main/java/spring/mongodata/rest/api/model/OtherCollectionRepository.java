/**
 * 
 */
package spring.mongodata.rest.api.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import spring.mongodata.rest.api.entity.OtherCollection;

/**
 * @author termigote
 *
 */
public interface OtherCollectionRepository extends MongoRepository<OtherCollection, String> {
    OtherCollection findBy_id(ObjectId id);

}

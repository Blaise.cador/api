/**
 * 
 */
package spring.mongodata.rest.api.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import spring.mongodata.rest.api.entity.Collection;

/**
 * @author termigote
 *
 */
public interface CollectionRepository extends MongoRepository<Collection, String> {
    Collection findBy_id(ObjectId id);

}

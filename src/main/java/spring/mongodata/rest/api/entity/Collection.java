package spring.mongodata.rest.api.entity;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Collection {

    @Id
    public ObjectId _id;

    public String firstparam;
    public String secondparam;
    public List<EmbededObject> embededObjects = new ArrayList<>();

    public Collection() {
    }

    public Collection(final ObjectId id, final String firstparam, final String secondparam, final List<EmbededObject> embededObjects) {
	this._id = id;
	this.firstparam = firstparam;
	this.secondparam = secondparam;
	this.embededObjects = embededObjects;
    }

    /**
     * @return the _id
     */
    public ObjectId get_id() {
	return this._id;
    }

    /**
     * @param _id the _id to set
     */
    public void set_id(final ObjectId _id) {
	this._id = _id;
    }

    /**
     * @return the firstparam
     */
    public String getFirstparam() {
	return this.firstparam;
    }

    /**
     * @param firstparam the firstparam to set
     */
    public void setFirstparam(final String firstparam) {
	this.firstparam = firstparam;
    }

    /**
     * @return the secondparam
     */
    public String getSecondparam() {
	return this.secondparam;
    }

    /**
     * @param secondparam the secondparam to set
     */
    public void setSecondparam(final String secondparam) {
	this.secondparam = secondparam;
    }

    /**
     * @return the embededObjects
     */
    public List<EmbededObject> getEmbededObjects() {
	return this.embededObjects;
    }

    /**
     * @param embededObjects the embededObjects to set
     */
    public void setEmbededObjects(final List<EmbededObject> embededObjects) {
	this.embededObjects = embededObjects;
    }

}

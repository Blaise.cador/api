package spring.mongodata.rest.api.entity;

public class EmbededObject {

    public String firstparam;
    public String secondparam;

    public EmbededObject() {
    }

    public EmbededObject(final String firstparam, final String secondparam) {
	this.firstparam = firstparam;
	this.secondparam = secondparam;
    }

    /**
     * @return the firstparam
     */
    public String getFirstparam() {
	return this.firstparam;
    }

    /**
     * @param firstparam the firstparam to set
     */
    public void setFirstparam(final String firstparam) {
	this.firstparam = firstparam;
    }

    /**
     * @return the secondparam
     */
    public String getSecondparam() {
	return this.secondparam;
    }

    /**
     * @param secondparam the secondparam to set
     */
    public void setSecondparam(final String secondparam) {
	this.secondparam = secondparam;
    }

}

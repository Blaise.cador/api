/**
 * 
 */
package spring.mongodata.rest.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.mongodata.rest.api.entity.OtherCollection;
import spring.mongodata.rest.api.model.OtherCollectionRepository;

/**
 * @author termigote
 *
 */
@RestController
@RequestMapping("/othercollections")
public class OtherCollectionController {

    @Autowired
    private OtherCollectionRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<OtherCollection> getAllOtherCollection() {
	return this.repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public OtherCollection getOtherCollectionById(@PathVariable("id") final ObjectId id) {
	return this.repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyOtherCollectionById(@PathVariable("id") final ObjectId id,
	    @Valid @RequestBody final OtherCollection otherCollection) {
	otherCollection.set_id(id);
	this.repository.save(otherCollection);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public OtherCollection createCollection(@Valid @RequestBody final OtherCollection otherCollection) {
	otherCollection.set_id(ObjectId.get());
	this.repository.save(otherCollection);
	return otherCollection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteCollection(@PathVariable final ObjectId id) {
	this.repository.delete(this.repository.findBy_id(id));
    }

}

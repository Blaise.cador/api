/**
 * 
 */
package spring.mongodata.rest.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.mongodata.rest.api.entity.Collection;
import spring.mongodata.rest.api.model.CollectionRepository;

/**
 * @author termigote
 *
 */
@RestController
@RequestMapping("/collections")
public class CollectionController {

    @Autowired
    private CollectionRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Collection> getAllCollection() {
	return this.repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Collection getCollectionById(@PathVariable("id") final ObjectId id) {
	return this.repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyCollectionById(@PathVariable("id") final ObjectId id, @Valid @RequestBody final Collection collection) {
	collection.set_id(id);
	this.repository.save(collection);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Collection createCollection(@Valid @RequestBody final Collection collection) {
	collection.set_id(ObjectId.get());
	this.repository.save(collection);
	return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteCollection(@PathVariable final ObjectId id) {
	this.repository.delete(this.repository.findBy_id(id));
    }

}
